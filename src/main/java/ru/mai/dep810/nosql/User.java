package ru.mai.dep810.nosql;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Persistent;
import org.springframework.data.mongodb.core.mapping.Field;

@Persistent
public class User {
    @Id
    private String id;
    @Field("CreationDate")
    private String creationDate;
    @Field("Views")
    private String views;
    @Field("DisplayName")
    private String displayName;
    @Field("Location")
    private String location;

    public User() {
    }

    public User(String id, String creationDate, String views, String displayName, String location) {
        this.id = id;
        this.creationDate = creationDate;
        this.views = views;
        this.displayName = displayName;
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
