package ru.mai.dep810.nosql;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;

@SpringBootApplication
public class Application {

    @Bean
    public MongoTemplate mongoTemplate(
            @Value("${mongo.url}") String url,
            @Value("${mongo.database}") String database
    ) {
        MongoClient mongoClient = MongoClients.create(url);
        return new MongoTemplate(mongoClient, database);
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
