package ru.mai.dep810.nosql;

import java.util.List;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

//    private Map<String, User> users = new HashMap<>();

    private MongoUserRepository mongoUserRepository;

    public UserController(MongoUserRepository mongoUserRepository) {
        this.mongoUserRepository = mongoUserRepository;
    }

//    public UserController() {
////        users.put("0", new User("0", Instant.now().toString(),
////                "10", "User #1", "Moscow"));
//       // mongoUserRepository = new MongoUserRepository();
//    }

    // GET /user
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public List<User> users() {
        return mongoUserRepository.findAll();
    }

    // GET /user/123
    @RequestMapping(value = "/user/{id}")
    public User getUser(@PathVariable("id") String id) {
        return mongoUserRepository.getById(id);
    }

    // POST /user
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public User createUser(@RequestBody User user) {
        return mongoUserRepository.saveOrUpdate(user);
    }

    // PUT /user/123
    @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
    public User saveUser(@PathVariable("id") String id, @RequestBody User user) {
        user.setId(id);
        return mongoUserRepository.saveOrUpdate(user);
    }

    // DELETE /user/123
    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    public void deleteUser(@PathVariable("id") String id) {
        mongoUserRepository.delete(id);
    }
}
