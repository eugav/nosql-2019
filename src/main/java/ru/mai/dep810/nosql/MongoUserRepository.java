package ru.mai.dep810.nosql;

import com.mongodb.client.result.DeleteResult;
import java.util.List;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class MongoUserRepository {

    private static final String COLLECTION_USERS = "Users";
    private final MongoTemplate template;

    public MongoUserRepository(MongoTemplate template) {
        this.template = template;
    }

    public List<User> findAll() {
        return template.findAll(User.class, COLLECTION_USERS);
    }

    public User getById(String id) {
        Query queryById = Query.query(Criteria.where("_id").is(id));
        return template.findOne(queryById, User.class, COLLECTION_USERS);
    }

    public User saveOrUpdate(User user) {
        return template.save(user, COLLECTION_USERS);
    }

    public void delete(String id) {
        Query queryById = Query.query(Criteria.where("_id").is(id));
        DeleteResult result = template.remove(queryById, COLLECTION_USERS);
        if (result.getDeletedCount() != 1) {
            throw new IllegalStateException("Cannot delete user with id = '" + id + "'");
        }
    }
}
